#Shell Details;
$shellName = "BlackXshell"; #Shell Name
$shellVersion = "Version: 0.1.0"; #version number
$shellBuild = "Build 002";
function prompt {
    $myName = $env:USERNAME #Username for this computer
    $currentFolder = Split-Path -Leaf -Path (Get-Location);

    $userName = Write-Host $myName -NoNewline -ForegroundColor Green;
    $separator = Write-Host " => " -NoNewline -ForegroundColor White;
    $displayFolder = Write-Host $currentFolder -NoNewline -ForegroundColor Black -BackgroundColor Green;
	
    $currentGitBranch = &git rev-parse --abbrev-ref HEAD
    $branchName = Write-Host " " $currentGitBranch -NoNewline -ForegroundColor Magenta;

    return "$displayFolder$separator$userName$branchName> ";
}

#Function to print an error if option command is invalid
function optionError {
    Param($option);
    Write-Host "Error: $option is not a valid option. Use the --help option to get a list of commands to use" -ForegroundColor Red;
    Write-Host;
}

function onStartMessage {
    Write-Host "~ $shellName is active ~" -ForegroundColor Green;
    Write-Host "";
}

function SetUpAliases {
    Set-Alias open start -Scope Global; #Creating an alias for start command as open
    Set-Alias chrome 'C:\Program Files (x86)\Google\Chrome\Application\chrome.exe' -Scope Global #Setting up chrome
}

# Function for dotnet clean, build and run
function dotnet_start {
    Clear-Host; dotnet clean; dotnet build --force; dotnet watch run;
}

#Function to restart powershell
function restart {
    powershell; 
    Write-Host "Powershell restarted successfully." -NoNewline -ForegroundColor Yellow;
}

#Function to check for blackxshell updates
function xshellUpdate {
    Write-Host "$shellName is up to date" -ForegroundColor Green;
    Write-Host "$shellVersion" -ForegroundColor Yellow;
}

#Function to list available blackxshell options
function xshellOptions {
    Write-Host $shellName;
    Write-Host $shellVersion;
    Write-Host $shellBuild;
    Write-Host "";

    Write-Host " --version; -v: Displays the $shellName version number.";
    Write-Host " update: Checks for the latest version of $shellName and applies update if available."
    Write-Host " --help; -h: List all the available $shellName options."
}
function blackxshell {
    #command option value
    $optionCommand = $args[0];

    #command options
    $versionCommand = "--version";
    $installCommand = "install";
    $updateCommand = "update";
    $helpCommand = "--help";
	
    #abbriviated option commands
    $abrVersionCommand = "-v";
    $abrHelpCommand = "-h";

    if ($optionCommand -eq $versionCommand -or $optionCommand -eq $abrVersionCommand) {
        Write-Host "$shellName $shellVersion" -ForegroundColor White;
    }
    elseif ($optionCommand -eq $installCommand) {
        $package = $args[1];

        if ($null -eq $package) {
            Write-Host "Error: Install option expects an argument" -ForegroundColor Red;
        }
        else {
            Write-Host "Package $package doesn't exist" -ForegroundColor Red;
        }
    }
    elseif ($optionCommand -eq $updateCommand) {
        xshellUpdate;
    }
    elseif ($null -eq $optionCommand) {
        Write-Host "$shellName $shellBuild";
        Write-Host "$shellVersion";
        Write-Host "";	
    }
    elseif ($optionCommand -eq $helpCommand -or $optionCommand -eq $abrHelpCommand) {
        xshellOptions;
    }
    else {
        optionError($optionCommand);
    }
}

#Function to get powershell colors
function getColors {
    $optionCommand = $args[0];
    $noColorOption = "--nocolor"

    if ($optionCommand -eq $noColorOption) {
        [System.Enum]::GetValues('ConsoleColor') | Write-Host;
    }
    elseif ($null -ne $optionCommand -and $optionCommand -ne $noColorOption) {
        optionError($optionCommand);
    }
    else {
        [System.Enum]::GetValues('ConsoleColor') | ForEach-Object { Write-Host  $_ -ForegroundColor $_ };
    }
}

function aliasForStartCommand {
    Set-Alias open start -Scope Global;
}
#Functions for dotnet commands
function clean {
    dotnet clean
}

function rebuild {
    dotnet build --force
}

function test {
    Write-Host "Script is working" -NoNewline -ForegroundColor Yellow;
}

function SetStartUpItems {
    SetUpAliases; #Initializing SetUpAliases function
    onStartMessage; # Display shell message
}

SetStartUpItems; #Initializing start up items