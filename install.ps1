#Script to install blackxshell

$blackxshellScript = "blackxshell.ps1";
$blackxshellName = "BlackXshell";

#Check if profile has been created
$profileExists = Test-Path $profile;

function createProfile {
    new-item -path $profile -itemtype file -force;
}

#function to copy blackxshell content to profile file
function copyContentToProfile {
    Get-Content $blackxshellScript | Out-File $profile;
}

#Function to restart powershell 
function restartPowershell {
    powershell
}

if ($profileExists) {
    Write-Host "File Found";
    Write-Host "Note: Your current profile will be over-written." -ForegroundColor Yellow;
    Write-Host "Installing $blackxshellName" -ForegroundColor Yellow;
    copyContentToProfile;
    Write-Host "$blackxshellName installed." -ForegroundColor Green;
    restartPowershell;
}
else {
    Write-Host "Profile Not Created" -ForegroundColor Red;
    Write-Host "Creating Profile" -ForegroundColor Yellow;
    createProfile;
    Write-Host "Profile created successfully" -ForegroundColor Green;
    Write-Host "Installing $blackxshellName" -ForegroundColor Yellow;
    copyContentToProfile;
    Write-Host "$blackxshellName installed." -ForegroundColor Green;
    restartPowershell;
}