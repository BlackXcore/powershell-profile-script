# Powershell Profile Script

A Powershell script to make your powershell prompt super cool. Install now! 😂😂😂

## Prerequisites
* Git is required. Download link: https://git-scm.com/

## Instructions:

You can either use the installation script to install BlackXshell or follow option 2 instructions (**Not recommended!**😂)

# Option 1 (Use installation script)

## Step 1
* Clone the repo to your computer and navigate to the folder.

eg.
```bash
cd powershell-profile-script
```


## Step 2 (Set the execution policy in powershell by running  the command below)
```bash
Set-ExecutionPolicy unrestricted -Scope CurrentUser
```

## Step 3 (Run the install script)

```bash
.\install.ps1 
```

# Option 2 (Manual)

## Step 1 - Test if there is a profile set up
```bash
test-path $profile
```

## Step 2 (If step 1 returned false else continue to step 3)
```bash
new-item -path $profile -itemtype file -force
```

## Step 3 (Use the below command to open the profile and paste the code in the powershell script)

*  The code is in the file called Microsoft.PowerShell_profile.ps1 in this repo
*  Change the "myName" variable value from BlackX to whatever you want
```bash
notepad $profile
```

## Step 4 (Save the script)
You shouldn't have to chose a location, it is already done for you🙌

## Step 5 (Set the execution policy in powershell by running  the command below)
```bash
Set-ExecutionPolicy unrestricted -Scope CurrentUser
```
 
## Step 6 
restart powershell

## Heal Yeah!🙌